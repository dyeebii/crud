﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace cris.Utilities
{
    class DAL : IDisposable
    {
        MySqlConnection conn;

        public DAL()
        {
            Connect();
        }

        private void Connect()
        {
            conn = new MySqlConnection();
            conn.ConnectionString = "server='" + DBConn.SERVERNAME + "'; database='" + DBConn.DB_NAME + "'; user id='" + DBConn.DB_USERNAME + "'; password='" + DBConn.DB_PASSWORD + "'";

            conn.Open();
        }
        public MySqlConnection connection { get { return conn; } }

        public void Dispose()
        {
            conn.Close();
        }

        //for running stored procs with SELECT statements ( or queries with result set)
        public MySqlDataReader ExecuteQuery(string spName, MySqlParameter[] param = null)
        {
            MySqlCommand cmd = new MySqlCommand(spName, conn);
            cmd.CommandType = CommandType.Text;
            if (param != null)
                cmd.Parameters.AddRange(param);

            MySqlDataReader reader = cmd.ExecuteReader();

            return reader;
        }

        //for running stored procs with INSERT/UPDATE/DELETE statements (or queries without result set)
        public void ExecuteNonQuery(string spName, MySqlParameter[] param = null)
        {
            using (MySqlCommand cmd = new MySqlCommand(spName, conn))
            {
                cmd.CommandType = CommandType.Text;
                if (param != null)
                    cmd.Parameters.AddRange(param);

                cmd.ExecuteNonQuery();
            }
        }

        public int ExecuteScalar(string spName, string tablename, MySqlParameter[] param = null)
        {
            using (MySqlCommand cmd = new MySqlCommand(spName + "; SELECT MAX(id) as id FROM " + tablename, conn))
            {
                cmd.CommandType = CommandType.Text;
                if (param != null)
                    cmd.Parameters.AddRange(param);

                int id = 0;
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0;

                return id;
            }
        }
        /*
        public int ExecuteScalar(string spName, MySqlParameter[] param = null)
        {
            using (MySqlCommand cmd = new MySqlCommand(spName.Substring(0, spName.IndexOf("values")) + " output INSERTED.ID " +
                spName.Substring(spName.IndexOf("values")), conn))
            {
                cmd.CommandType = CommandType.Text;
                if (param != null)
                    cmd.Parameters.AddRange(param);

                return (int)cmd.ExecuteScalar();
            }
        }
        */
    }
}
