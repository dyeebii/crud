﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    public partial class Form1 : Form
    {
        List<User> user_list;
        User selected_user;
        public Form1()
        {
            InitializeComponent();
            user_list = new List<User>();

        }
        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(txtSearch.Text.Length > 0)
            {
                btnSearch.Enabled = true;
               

            }
            else
            {
                btnSearch.Enabled = false;
                GetData();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(txtName.Text.Length > 0)
            {
                User user = new User();
                
                user.name = txtName.Text;

                var id = UserController.Save(user);
                if (id != 0)
                    MessageBox.Show("User was saved");

                GetData();
                ClearForm();
            }
        }

        private void ClearForm()
        {
            txtID.Text = "";
            txtName.Text = "";
            btnAdd.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void lvItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvItems.SelectedItems.Count > 0)
            {
                ListViewItem lvi = lvItems.SelectedItems[0];
                txtID.Text = lvi.SubItems[0].Text;
                txtName.Text = lvi.SubItems[1].Text;

                selected_user = new User();
                selected_user.id = Convert.ToInt32(txtID.Text);
                selected_user.name = txtName.Text;

                btnAdd.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            ClearForm();
            btnAdd.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            GetData();
        }

        private void GetData()
        {
            user_list = new List<User>();
            user_list = UserController.GetAll();
            lvItems.Items.Clear();
            foreach (var user in user_list)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = user.id.ToString();
                lvi.SubItems.Add(user.name);
                lvItems.Items.Add(lvi);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            selected_user.name = txtName.Text;
            if (UserController.Update(selected_user))
                MessageBox.Show("User was updated");

            GetData();
            ClearForm();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (UserController.Delete(selected_user.id))
                MessageBox.Show("User was deleted");

            GetData();
            ClearForm();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            user_list = new List<User>();
            user_list = UserController.GetByName(txtSearch.Text);
            lvItems.Items.Clear();
            foreach (var user in user_list)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = user.id.ToString();
                lvi.SubItems.Add(user.name);
                lvItems.Items.Add(lvi);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            btnSearch.Enabled = false;
            GetData();
        }
    }
}
