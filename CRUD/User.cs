﻿using cris.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    class User
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    class UserController
    {
        private const string tablename = "users";

        public static int Save(User item)
        {
            int id = 0;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "insert into " + tablename + 
                        " (name)" +
                        " values(@name)";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@name", item.name),
                    };

                    id = dal.ExecuteScalar(query, tablename, param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                id = 0;
            }

            return id;
        }

        public static bool Update(User item)
        {
            bool updated = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set name=@name" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", item.id),
                        new MySqlParameter("@name", item.name),
                    };

                    dal.ExecuteNonQuery(query, param); 
                }
                updated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                updated = false;
            }

            return updated;
        }

        public static bool Delete(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "DELETE FROM " + tablename +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }



        private static User GetPopulate(MySqlDataReader reader)
        {
            var item = new User()
            {
                id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0,
                name = reader["name"] != null ? reader["name"].ToString() : "",
            };
            return item;
        }

        public static User GetById(int id)
        {
            User item = null;
            try
            {
                item = new User();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<User> GetAll()
        {
            List<User> items = null;
            try
            {
                items = new List<User>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename;
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        User item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static List<User> GetByName(string name)
        {
            List<User> items = null;
            try
            {
                items = new List<User>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where name like '%"+name+"%'";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        User item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

    }
}
