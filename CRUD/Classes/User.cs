﻿using cris.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace cris.Classes
{
    class User : BaseClass
    {
        public string remember_token { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string role { get; set; }
    }

    class UserController
    {
        private const string tablename = "users";

        public static int Save(User item)
        {
            int id = 0;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "insert into " + tablename + 
                        " (username, password, role, created_at)" +
                        " values(@username, SHA1(@password), @role, @created_at) ";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@username", item.username),
                        new MySqlParameter("@password", item.password),
                        new MySqlParameter("@role", item.role),
                        new MySqlParameter("@created_at", DateTime.Now),
                    };

                    id = dal.ExecuteScalar(query, tablename, param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                id = 0;
            }

            return id;
        }

        public static bool Update(User item)
        {
            bool updated = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set username=@username, password=SHA1(@password), remember_token =@remember_token, role=@role, updated_at=@updated_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", item.id),
                        new MySqlParameter("@username", item.username),
                        new MySqlParameter("@password", item.password),
                        new MySqlParameter("@remember_token", item.remember_token),
                        new MySqlParameter("@role", item.role),
                        new MySqlParameter("@updated_at", DateTime.Now),
                    };

                    dal.ExecuteNonQuery(query, param); 
                }
                updated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                updated = false;
            }

            return updated;
        }

        public static bool Delete(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename + 
                        " set deleted_at=@deleted_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@deleted_at", DateTime.Now),
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        public static bool Restore(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=null" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        private static User GetPopulate(MySqlDataReader reader)
        {
            var item = new User()
            {
                id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0,
                username = reader["username"] != null ? reader["username"].ToString() : "",
                password = reader["password"] != null ? reader["password"].ToString() : "",
                role = reader["role"] != null ? reader["role"].ToString() : "",
                created_at = reader["created_at"] != null ? reader["created_at"].ToString() : "",
                updated_at = reader["updated_at"] != null ? reader["updated_at"].ToString() : "",
                deleted_at = reader["deleted_at"] != null ? reader["deleted_at"].ToString() : "",
            };
            return item;
        }

        public static User GetById(int id)
        {
            User item = null;
            try
            {
                item = new User();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static User GetByToken(string token)
        {
            User item = null;
            try
            {
                item = new User();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where remember_token=@token and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@token", token) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<User> GetAll()
        {
            List<User> items = null;
            try
            {
                items = new List<User>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where ISNULL(deleted_at)";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        User item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static User GetByIdDeleted(int id)
        {
            User item = null;
            try
            {
                item = new User();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)=false";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<User> GetAllDeleted()
        {
            List<User> items = null;
            try
            {
                items = new List<User>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where  ISNULL(deleted_at)=false";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        var item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static User Login(string username, string password)
        {
            User item = null;
            try
            {
                item = new User();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where username=@username and password=SHA1(@password) and ISNULL(deleted_at)";
                    MySqlParameter[] param = 
                    {
                        new MySqlParameter("@username", username),
                        new MySqlParameter("@password", password)
                    };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }
    }
}
