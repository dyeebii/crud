﻿using cris.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace cris.Classes
{
    class DeathCert : CertBaseClass
    {
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string suffix { get; set; }
        public char sex { get; set; }
        public DateTime dob { get; set; }
        public DateTime dod { get; set; }
        public string pob_city_municipality { get; set; }
        public string pob_province { get; set; }
        public string pob_country { get; set; }
    }

    class DeathCertController
    {
        private const string tablename = "death_certs";

        public static int Save(DeathCert item)
        {
            int id = 0;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "insert into " + tablename +
                        " (firstname, middlename, lastname, suffix, sex," +
                        " dob, dod, pob_city_municipality, pob_province, pob_country, registry_no, created_at)" +
                        " values(@firstname, @middlename, @lastname, @suffix, @sex," +
                        " @dob, @dod, @pob_city_municipality, @pob_province, @pob_country, @registry_no, @created_at)";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@firstname", item.firstname),
                        new MySqlParameter("@middlename", item.middlename),
                        new MySqlParameter("@lastname", item.lastname),
                        new MySqlParameter("@suffix", item.suffix),
                        new MySqlParameter("@sex", item.sex),
                        new MySqlParameter("@dob", item.dob),
                        new MySqlParameter("@dod", item.dod),
                        new MySqlParameter("@pob_city_municipality", item.pob_city_municipality),
                        new MySqlParameter("@pob_province", item.pob_province),
                        new MySqlParameter("@pob_country", item.pob_country),
                        new MySqlParameter("@registry_no", item.registry_no),
                        new MySqlParameter("@created_at", DateTime.Now),
                    };

                    id = dal.ExecuteScalar(query, tablename, param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                id = 0;
            }

            return id;
        }

        public static bool Update(DeathCert item)
        {
            bool updated = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set firstname=@firstname, middlename=@middlename, lastname=@lastname, suffix=@suffix, sex=@sex," +
                        " dob=@dob, dod=@dod, pob_city_municipality=@pob_city_municipality, pob_province=@pob_province, pob_country=@pob_country, updated_at=@updated_at, registry_no=@registry_no" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", item.id),
                        new MySqlParameter("@firstname", item.firstname),
                        new MySqlParameter("@middlename", item.middlename),
                        new MySqlParameter("@lastname", item.lastname),
                        new MySqlParameter("@suffix", item.suffix),
                        new MySqlParameter("@sex", item.sex),
                        new MySqlParameter("@dob", item.dob),
                        new MySqlParameter("@dod", item.dod),
                        new MySqlParameter("@pob_city_municipality", item.pob_city_municipality),
                        new MySqlParameter("@pob_province", item.pob_province),
                        new MySqlParameter("@pob_country", item.pob_country),
                        new MySqlParameter("@registry_no", item.registry_no),
                        new MySqlParameter("@updated_at", DateTime.Now),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                updated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                updated = false;
            }

            return updated;
        }

        public static bool Delete(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=@deleted_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@deleted_at", DateTime.Now),
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        public static bool Restore(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=null" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        private static DeathCert GetPopulate(MySqlDataReader reader)
        {
            var item = new DeathCert()
            {
                id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0,
                firstname = reader["firstname"] != null ? reader["firstname"].ToString() : "",
                middlename = reader["middlename"] != null ? reader["middlename"].ToString() : "",
                lastname = reader["lastname"] != null ? reader["lastname"].ToString() : "",
                suffix = reader["suffix"] != null ? reader["suffix"].ToString() : "",
                sex = reader["sex"] != null ? Convert.ToChar(reader["sex"].ToString()) : Convert.ToChar("F"),
                dob = reader["dob"] != null ? Convert.ToDateTime(reader["dob"].ToString()) : new DateTime(),
                dod = reader["dod"] != null ? Convert.ToDateTime(reader["dod"].ToString()) : new DateTime(),
                pob_city_municipality = reader["pob_city_municipality"] != null ? reader["pob_city_municipality"].ToString() : "",
                pob_province = reader["pob_province"] != null ? reader["pob_province"].ToString() : "",
                pob_country = reader["pob_country"] != null ? reader["pob_country"].ToString() : "",
                registry_no = reader["registry_no"] != null ? reader["registry_no"].ToString() : "",
                created_at = reader["created_at"] != null ? reader["created_at"].ToString() : "",
                updated_at = reader["updated_at"] != null ? reader["updated_at"].ToString() : "",
                deleted_at = reader["deleted_at"] != null ? reader["deleted_at"].ToString() : "",
            };
            return item;
        }

        public static DeathCert GetById(int id)
        {
            DeathCert item = null;
            try
            {
                item = new DeathCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<DeathCert> GetAll()
        {
            List<DeathCert> items = null;
            try
            {
                items = new List<DeathCert>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where ISNULL(deleted_at)";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        DeathCert item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static DeathCert GetByIdDeleted(int id)
        {
            DeathCert item = null;
            try
            {
                item = new DeathCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)=false";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<DeathCert> GetAllDeleted()
        {
            List<DeathCert> items = null;
            try
            {
                items = new List<DeathCert>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where  ISNULL(deleted_at)=false";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        var item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }
    }
}
