﻿using cris.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace cris.Classes
{
    class Request : BaseClass
    {
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string suffix { get; set; }
        public int no_of_copies { get; set; }
        public string status { get; set; }
        public string relationship { get; set; }
        public string purpose { get; set; }
        public int cert_id { get; set; }
        public string cert_type { get; set; }
    }

    class RequestController
    {
        private const string tablename = "requests";

        public static int Save(Request item)
        {
            int id = 0;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "insert into " + tablename +
                        " (firstname, middlename, lastname, suffix, no_of_copies," +
                        " status, relationship, purpose, cert_id, cert_type, created_at)" +
                        " values(@firstname, @middlename, @lastname, @suffix, @no_of_copies," +
                        " @status, @relationship, @purpose, @cert_id, @cert_type, @created_at)";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@firstname", item.firstname),
                        new MySqlParameter("@middlename", item.middlename),
                        new MySqlParameter("@lastname", item.lastname),
                        new MySqlParameter("@suffix", item.suffix),
                        new MySqlParameter("@no_of_copies", item.no_of_copies),
                        new MySqlParameter("@status", item.status),
                        new MySqlParameter("@relationship", item.relationship),
                        new MySqlParameter("@purpose", item.purpose),
                        new MySqlParameter("@cert_id", item.cert_id),
                        new MySqlParameter("@cert_type", item.cert_type),
                        new MySqlParameter("@created_at", DateTime.Now),
                    };

                    id = dal.ExecuteScalar(query, tablename, param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                id = 0;
            }

            return id;
        }

        public static bool Update(Request item)
        {
            bool updated = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set firstname=@firstname, middlename=@middlename, lastname=@lastname, suffix=@suffix, no_of_copies=@no_of_copies," +
                        " status=@status, relationship=@relationship, purpose=@purpose, cert_id=@cert_id, cert_type=@cert_type, updated_at=@updated_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", item.id),
                        new MySqlParameter("@firstname", item.firstname),
                        new MySqlParameter("@middlename", item.middlename),
                        new MySqlParameter("@lastname", item.lastname),
                        new MySqlParameter("@suffix", item.suffix),
                        new MySqlParameter("@no_of_copies", item.no_of_copies),
                        new MySqlParameter("@status", item.status),
                        new MySqlParameter("@relationship", item.relationship),
                        new MySqlParameter("@purpose", item.purpose),
                        new MySqlParameter("@cert_id", item.cert_id),
                        new MySqlParameter("@cert_type", item.cert_type),
                        new MySqlParameter("@updated_at", DateTime.Now),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                updated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                updated = false;
            }

            return updated;
        }

        public static bool Delete(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=@deleted_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@deleted_at", DateTime.Now),
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        public static bool Restore(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=null" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        private static Request GetPopulate(MySqlDataReader reader)
        {
            var item = new Request()
            {
                id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0,
                firstname = reader["firstname"] != null ? reader["firstname"].ToString() : "",
                middlename = reader["middlename"] != null ? reader["middlename"].ToString() : "",
                lastname = reader["lastname"] != null ? reader["lastname"].ToString() : "",
                suffix = reader["suffix"] != null ? reader["suffix"].ToString() : "",
                no_of_copies = reader["no_of_copies"] != null ? Convert.ToInt32(reader["no_of_copies"].ToString()) : 0,
                status = reader["status"] != null ? reader["status"].ToString() : "",
                relationship = reader["relationship"] != null ? reader["relationship"].ToString() : "",
                purpose = reader["purpose"] != null ? reader["purpose"].ToString() : "",
                cert_id = reader["cert_id"] != null ? Convert.ToInt32(reader["cert_id"].ToString()) : 0,
                cert_type = reader["cert_type"] != null ? reader["cert_type"].ToString() : "",
                created_at = reader["created_at"] != null ? reader["created_at"].ToString() : "",
                updated_at = reader["updated_at"] != null ? reader["updated_at"].ToString() : "",
                deleted_at = reader["deleted_at"] != null ? reader["deleted_at"].ToString() : "",
            };
            return item;
        }

        public static Request GetById(int id)
        {
            Request item = null;
            try
            {
                item = new Request();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<Request> GetAll()
        {
            List<Request> items = null;
            try
            {
                items = new List<Request>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where ISNULL(deleted_at)";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        Request item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static Request GetByIdDeleted(int id)
        {
            Request item = null;
            try
            {
                item = new Request();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)=false";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<Request> GetAllDeleted()
        {
            List<Request> items = null;
            try
            {
                items = new List<Request>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where  ISNULL(deleted_at)=false";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        var item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }
    }
}
