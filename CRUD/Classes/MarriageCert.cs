﻿using cris.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace cris.Classes
{
    class MarriageCert : CertBaseClass
    {
        public string husband_firstname { get; set; }
        public string husband_middlename { get; set; }
        public string husband_lastname { get; set; }
        public string husband_suffix { get; set; }
        public DateTime husband_dob { get; set; }
        public string husband_tin { get; set; }
        public string wife_firstname { get; set; }
        public string wife_middlename { get; set; }
        public string wife_lastname { get; set; }
        public string wife_suffix { get; set; }
        public DateTime wife_dob { get; set; }
        public string wife_tin { get; set; }
        public DateTime dom { get; set; }
        public string pom_city_municipality { get; set; }
        public string pom_province { get; set; }
        public string pom_country { get; set; }
    }

    class MarriageCertController
    {
        private const string tablename = "marriage_certs";

        public static int Save(MarriageCert item)
        {
            int id = 0;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "insert into " + tablename +
                        " (husband_firstname, husband_middlename, husband_lastname, husband_suffix, husband_dob, husband_tin," +
                        " wife_firstname, wife_middlename, wife_lastname, wife_suffix, wife_dob, wife_tin," +
                        " dom, pom_city_municipality, pom_province, pom_country, registry_no, created_at)" +
                        " values(@husband_firstname, @husband_middlename, @husband_lastname, @husband_suffix, @husband_dob, @husband_tin," +
                        " @wife_firstname, @wife_middlename, @wife_lastname, @wife_suffix, @wife_dob, @wife_tin," +
                        " @dom, @pom_city_municipality, @pom_province, @pom_country, @registry_no @created_at)";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@husband_firstname", item.husband_firstname),
                        new MySqlParameter("@husband_middlename", item.husband_middlename),
                        new MySqlParameter("@husband_lastname", item.husband_lastname),
                        new MySqlParameter("@husband_suffix", item.husband_suffix),
                        new MySqlParameter("@husband_dob", item.husband_dob),
                        new MySqlParameter("@husband_tin", item.husband_tin),
                        new MySqlParameter("@wife_firstname", item.wife_firstname),
                        new MySqlParameter("@wife_middlename", item.wife_middlename),
                        new MySqlParameter("@wife_lastname", item.wife_lastname),
                        new MySqlParameter("@wife_suffix", item.wife_suffix),
                        new MySqlParameter("@wife_dob", item.wife_dob),
                        new MySqlParameter("@wife_tin", item.wife_tin),
                        new MySqlParameter("@dom", item.dom),
                        new MySqlParameter("@pom_city_municipality", item.pom_city_municipality),
                        new MySqlParameter("@pom_province", item.pom_province),
                        new MySqlParameter("@pom_country", item.pom_country),
                        new MySqlParameter("@registry_no", item.registry_no),
                        new MySqlParameter("@created_at", DateTime.Now),
                    };

                    id = dal.ExecuteScalar(query, tablename, param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                id = 0;
            }

            return id;
        }

        public static bool Update(MarriageCert item)
        {
            bool updated = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set husband_firstname=@husband_firstname, husband_middlename=@husband_middlename, husband_lastname=@husband_lastname, husband_suffix=@husband_suffix, husband_dob=@husband_dob, husband_tin=@husband_tin," +
                        " wife_firstname=@wife_firstname, wife_middlename=@wife_middlename, wife_lastname=@wife_lastname, wife_suffix=@wife_suffix, wife_dob=@wife_dob, wife_tin=@wife_tin," +
                        " dom=@dom, pom_city_municipality=@pom_city_municipality, pom_province=@pom_province, pom_country=@pom_country, updated_at=@updated_at, registry_no=@registry_no" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", item.id),
                        new MySqlParameter("@husband_firstname", item.husband_firstname),
                        new MySqlParameter("@husband_middlename", item.husband_middlename),
                        new MySqlParameter("@husband_lastname", item.husband_lastname),
                        new MySqlParameter("@husband_suffix", item.husband_suffix),
                        new MySqlParameter("@husband_dob", item.husband_dob),
                        new MySqlParameter("@husband_tin", item.husband_tin),
                        new MySqlParameter("@wife_firstname", item.wife_firstname),
                        new MySqlParameter("@wife_middlename", item.wife_middlename),
                        new MySqlParameter("@wife_lastname", item.wife_lastname),
                        new MySqlParameter("@wife_suffix", item.wife_suffix),
                        new MySqlParameter("@wife_dob", item.wife_dob),
                        new MySqlParameter("@wife_tin", item.wife_tin),
                        new MySqlParameter("@dom", item.dom),
                        new MySqlParameter("@pom_city_municipality", item.pom_city_municipality),
                        new MySqlParameter("@pom_province", item.pom_province),
                        new MySqlParameter("@pom_country", item.pom_country),
                        new MySqlParameter("@registry_no", item.registry_no),
                        new MySqlParameter("@updated_at", DateTime.Now),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                updated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                updated = false;
            }

            return updated;
        }

        public static bool Delete(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=@deleted_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@deleted_at", DateTime.Now),
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        public static bool Restore(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=null" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        private static MarriageCert GetPopulate(MySqlDataReader reader)
        {
            var item = new MarriageCert()
            {
                id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0,
                husband_firstname = reader["husband_firstname"] != null ? reader["husband_firstname"].ToString() : "",
                husband_middlename = reader["husband_middlename"] != null ? reader["husband_middlename"].ToString() : "",
                husband_lastname = reader["husband_lastname"] != null ? reader["husband_lastname"].ToString() : "",
                husband_suffix = reader["husband_suffix"] != null ? reader["husband_suffix"].ToString() : "",
                husband_dob = reader["husband_dob"] != null ? Convert.ToDateTime(reader["husband_dob"].ToString()) : new DateTime(),
                husband_tin = reader["husband_tin"] != null ? reader["husband_tin"].ToString() : "",
                wife_firstname = reader["wife_firstname"] != null ? reader["wife_firstname"].ToString() : "",
                wife_middlename = reader["wife_middlename"] != null ? reader["wife_middlename"].ToString() : "",
                wife_lastname = reader["wife_lastname"] != null ? reader["wife_lastname"].ToString() : "",
                wife_suffix = reader["wife_suffix"] != null ? reader["wife_suffix"].ToString() : "",
                wife_dob = reader["wife_dob"] != null ? Convert.ToDateTime(reader["wife_dob"].ToString()) : new DateTime(),
                wife_tin = reader["wife_tin"] != null ? reader["wife_tin"].ToString() : "",
                dom = reader["dom"] != null ? Convert.ToDateTime(reader["dom"].ToString()) : new DateTime(),
                pom_city_municipality = reader["pom_city_municipality"] != null ? reader["pom_city_municipality"].ToString() : "",
                pom_province = reader["pom_province"] != null ? reader["pom_province"].ToString() : "",
                pom_country = reader["pom_country"] != null ? reader["pom_country"].ToString() : "",
                registry_no = reader["registry_no"] != null ? reader["registry_no"].ToString() : "",
                created_at = reader["created_at"] != null ? reader["created_at"].ToString() : "",
                updated_at = reader["updated_at"] != null ? reader["updated_at"].ToString() : "",
                deleted_at = reader["deleted_at"] != null ? reader["deleted_at"].ToString() : "",
            };
            return item;
        }

        public static MarriageCert GetById(int id)
        {
            MarriageCert item = null;
            try
            {
                item = new MarriageCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<MarriageCert> GetAll()
        {
            List<MarriageCert> items = null;
            try
            {
                items = new List<MarriageCert>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where ISNULL(deleted_at)";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        MarriageCert item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static MarriageCert GetByIdDeleted(int id)
        {
            MarriageCert item = null;
            try
            {
                item = new MarriageCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)=false";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<MarriageCert> GetAllDeleted()
        {
            List<MarriageCert> items = null;
            try
            {
                items = new List<MarriageCert>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where  ISNULL(deleted_at)=false";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        var item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }
    }
}
