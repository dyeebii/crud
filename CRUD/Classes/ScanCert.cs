﻿using cris.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace cris.Classes
{
    class ScanCert : BaseClass
    {
        public string file { get; set; }
        public string image { get; set; }
        public int cert_id { get; set; }
        public string cert_type { get; set; }
    }

    class ScanCertController
    {
        private const string tablename = "scan_certs";

        public static int Save(ScanCert item)
        {
            int id = 0;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "insert into " + tablename +
                        " (file, image, cert_id, cert_type, created_at)" +
                        " values(@file, @image, @cert_id, @cert_type,  @created_at) ";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@file", item.file),
                        new MySqlParameter("@image", item.image),
                        new MySqlParameter("@cert_id", item.cert_id),
                        new MySqlParameter("@cert_type", item.cert_type),
                        new MySqlParameter("@created_at", DateTime.Now),
                    };

                    id = dal.ExecuteScalar(query, tablename, param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                id = 0;
            }

            return id;
        }

        public static bool Update(ScanCert item)
        {
            bool updated = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set file=@file, image=@image, cert_id=@cert_id, cert_type=@cert_type, updated_at=@updated_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", item.id),
                        new MySqlParameter("@file", item.file),
                        new MySqlParameter("@image", item.image),
                        new MySqlParameter("@cert_id", item.cert_id),
                        new MySqlParameter("@cert_type", item.cert_type),
                        new MySqlParameter("@updated_at", DateTime.Now),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                updated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                updated = false;
            }

            return updated;
        }

        public static bool Delete(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=@deleted_at" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@deleted_at", DateTime.Now),
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        public static bool Restore(int id)
        {
            bool deleted = false;
            try
            {
                using (DAL dal = new DAL())
                {
                    string query = "update " + tablename +
                        " set deleted_at=null" +
                        " where id=@id";
                    MySqlParameter[] param =
                    {
                        new MySqlParameter("@id", id),
                    };

                    dal.ExecuteNonQuery(query, param);
                }
                deleted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                deleted = false;
            }

            return deleted;
        }

        private static ScanCert GetPopulate(MySqlDataReader reader)
        {
            var item = new ScanCert()
            {
                id = reader["id"] != null ? Convert.ToInt32(reader["id"].ToString()) : 0,
                file = reader["file"] != null ? reader["file"].ToString() : "",
                image = reader["image"] != null ? reader["image"].ToString() : "",
                cert_id = reader["cert_id"] != null ? Convert.ToInt32(reader["cert_id"].ToString()) : 0,
                cert_type = reader["cert_type"] != null ? reader["cert_type"].ToString() : "",
                created_at = reader["created_at"] != null ? reader["created_at"].ToString() : "",
                updated_at = reader["updated_at"] != null ? reader["updated_at"].ToString() : "",
                deleted_at = reader["deleted_at"] != null ? reader["deleted_at"].ToString() : "",
            };
            return item;
        }

        public static ScanCert GetById(int id)
        {
            ScanCert item = null;
            try
            {
                item = new ScanCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static ScanCert GetByCertificate(int id,string type)
        {
            ScanCert item = null;
            try
            {
                item = new ScanCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where cert_id=@id and cert_type=@type and ISNULL(deleted_at)";
                    MySqlParameter[] param = { new MySqlParameter("@id", id), new MySqlParameter("@type",type) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<ScanCert> GetAll()
        {
            List<ScanCert> items = null;
            try
            {
                items = new List<ScanCert>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where ISNULL(deleted_at)";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        ScanCert item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }

        public static ScanCert GetByIdDeleted(int id)
        {
            ScanCert item = null;
            try
            {
                item = new ScanCert();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where id=@id and ISNULL(deleted_at)=false";
                    MySqlParameter[] param = { new MySqlParameter("@id", id) };
                    MySqlDataReader reader = dal.ExecuteQuery(query, param);

                    while (reader.Read())
                    {
                        item = GetPopulate(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return item;
        }

        public static List<ScanCert> GetAllDeleted()
        {
            List<ScanCert> items = null;
            try
            {
                items = new List<ScanCert>();
                using (DAL dal = new DAL())
                {
                    string query = "select * from " + tablename +
                        " where  ISNULL(deleted_at)=false";
                    MySqlDataReader reader = dal.ExecuteQuery(query);

                    while (reader.Read())
                    {
                        var item = GetPopulate(reader);
                        items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return items;
        }
    }
}
