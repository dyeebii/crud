﻿using cris.Classes;

namespace cris.Utilities
{
    class Globals
    {
        private static mdiHome _main = null;
        public static mdiHome MAIN_FORM { get { return _main = _main == null ? new mdiHome() : _main; } }
        public static User CURRENT_USER { get; set; }
    }
}
